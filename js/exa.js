// Archivo: app1.js

// Función para realizar la petición utilizando Axios
function realizarPeticion() {
  // Obtener el valor del campo de texto con el ID 'text'
  const id = document.getElementById('text').value;

  // Validar que el campo no esté vacío
  if (!id) {
      console.error('Por favor, ingrese un ID antes de realizar la petición.');
      return;
  }

  // Realizar la petición GET a la API con Axios
  axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(response => {
          // Manejar la respuesta exitosa y llenar los campos del formulario
          const usuario = response.data;
          document.getElementById('nombre').value = usuario.name;
          document.getElementById('nombreUsuario').value = usuario.username;
          document.getElementById('email').value = usuario.email;

          // Acceder a las propiedades del objeto de dirección individualmente
          document.getElementById('Calle').value = usuario.address.street;
          document.getElementById('Numero').value = usuario.address.suite;
          document.getElementById('Ciudad').value = usuario.address.city;
      })
      .catch(error => {
          // Manejar el error en caso de que la petición no sea exitosa
          console.error('Error al realizar la petición:', error);
      });
      
}
