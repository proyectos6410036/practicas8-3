function buscarInformacion() {
    const nombrePaisInput = document.getElementById("nombrePais");
    const nombrePais = nombrePaisInput.value;

    // Validar si se ingresó un nombre de país
    if (nombrePais.trim() !== "") {
        // Consumir el servicio de restcountries.com con el nombre del país proporcionado
        fetch(`https://restcountries.com/v3.1/name/${nombrePais}`)
            .then(response => {
                // Verificar el estado de la respuesta
                if (!response.ok) {
                    throw new Error(`No se pudo obtener la información del país. Estado: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                // Verificar si se encontró el país
                if (data.length > 0) {
                    const countryInfo = data[0];
                    const capital = countryInfo.capital || "No disponible";
                    const lenguaje = obtenerLenguajes(countryInfo);

                    // Mostrar la información en el HTML
                    document.getElementById("capital").textContent = capital;
                    document.getElementById("lenguaje").textContent = lenguaje;
                } else {
                    alert("País no encontrado");
                }
            })
            .catch(error => {
                console.error("Error al obtener la información del país:", error);
            });
    } else {
        alert("Por favor, ingresa el nombre del país");
    }
}

function obtenerLenguajes(countryInfo) {
    if (countryInfo.languages) {
        // El servicio restcountries.com puede devolver idiomas de diferentes maneras, dependiendo del país
        if (Array.isArray(countryInfo.languages)) {
            return countryInfo.languages.map(lang => lang.name || "No disponible").join(", ");
        } else if (typeof countryInfo.languages === "object") {
            return Object.values(countryInfo.languages).join(", ");
        }
    }
    return "No disponible";
}

function limpiarFormulario() {
    document.getElementById("nombrePais").value = "";
    document.getElementById("capital").textContent = "";
    document.getElementById("lenguaje").textContent = "";
}
