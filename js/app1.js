function cargarDatos(Idc) {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums/" + Idc;
  
    http.onreadystatechange = function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          let res = document.getElementById('lista');
          const datos = JSON.parse(this.responseText);
          res.innerHTML = '';
  
          if (datos.id) {
            res.innerHTML += '<tr><td class="columna1">' + datos.userId + '</td>' +
              '<td class="columna2">' + datos.id + '</td>' +
              '<td class="columna3">' + datos.title + '</td></tr>';
          } else {
            alert("No se encontraron datos para el ID proporcionado.");
          }
        } else {
          alert("Error al cargar los datos. Por favor, inténtelo de nuevo.");
        }
      }
    };
  
    http.open('GET', url, true);
    http.send();
  }
  
  document.getElementById("btnBuscar").addEventListener('click', function () {
    const Idc = document.getElementById("Idc").value.trim();
  
    if (Idc !== "") {
      const isNumeric = /^\d+$/.test(Idc);
      if (isNumeric && parseInt(Idc) > 0) {
        cargarDatos(Idc);
      } else {
        alert("Por favor, ingrese un ID válido (número entero positivo).");
      }
    } else {
      alert("Por favor, ingrese un ID.");
    }
  });
  
  document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
  });
  