document.getElementById('cargarRazas').addEventListener('click', function () {
    // Hacer una solicitud para obtener la lista de razas
    fetch('https://dog.ceo/api/breeds/list')
        .then(response => response.json())
        .then(data => {
            // Obtener el elemento del combo box
            const comboBox = document.createElement('select');
            comboBox.setAttribute('id', 'razasCombo');
            
            // Agregar las opciones al combo box
            data.message.forEach(raza => {
                const option = document.createElement('option');
                option.value = raza;
                option.text = raza;
                comboBox.appendChild(option);
            });

            // Reemplazar el contenido de la sección de selección de razas con el combo box
            const seleccionRazaSection = document.getElementById('seleccionRaza');
            seleccionRazaSection.innerHTML = '<h2>Selecciona una raza</h2>';
            seleccionRazaSection.appendChild(comboBox);

            // Agregar el botón "Ver Imagen"
            const verImagenBtn = document.createElement('button');
            verImagenBtn.setAttribute('id', 'verImagenBtn');
            verImagenBtn.textContent = 'Ver Imagen';
            seleccionRazaSection.appendChild(verImagenBtn);

            // Agregar un evento al botón "Ver Imagen"
            verImagenBtn.addEventListener('click', function () {
                const selectedRaza = comboBox.value;
                cargarImagen(selectedRaza);
            });
        })
        .catch(error => console.error('Error al cargar las razas:', error));
});

function cargarImagen(raza) {
    // Hacer una solicitud para obtener una imagen aleatoria de la raza seleccionada
    fetch(`https://dog.ceo/api/breed/${raza}/images/random`)
        .then(response => response.json())
        .then(data => {
            // Actualizar la imagen en la sección de imagenPerro
            const cuadroImagen = document.getElementById('cuadroImagen');
            cuadroImagen.src = data.message;
            cuadroImagen.alt = `Imagen de un ${raza}`;
        })
        .catch(error => console.error('Error al cargar la imagen:', error));
        
}

