// script.js
function cargarDatos(Idc) {
  const http = new XMLHttpRequest();
  const url = "https://jsonplaceholder.typicode.com/users/" + Idc;

  http.onreadystatechange = function () {
    if (this.readyState === 4) {
      if (this.status === 200) {
        let res = document.getElementById('lista');
        const datos = JSON.parse(this.responseText);
        res.innerHTML = '';

        if (datos.id) {
          res.innerHTML += '<tr><td class="columna1">' + datos.id + '</td>' +
            '<td class="columna2">' + datos.name + '</td>' +
            '<td class="columna3">' + datos.email + '</td></tr>';
        } else {
          alert("No se encontraron datos para el ID proporcionado.");
        }
      } else {
        alert("Error al cargar los datos. Por favor, inténtelo de nuevo.");
      }
    }
  };

  http.open('GET', url, true);
  http.send();
}
document.getElementById("btnBuscar").addEventListener('click', function () {
  const Idc = document.getElementById("Idc").value.trim();

  if (Idc !== "") {
    const isNumeric = /^\d+$/.test(Idc);
    if (isNumeric && parseInt(Idc) > 0) {
      cargarDatos(Idc);
    } else {
      alert("Por favor, ingrese un ID válido (número entero positivo).");
    }
  } else {
    alert("Por favor, ingrese un ID.");
  }
});


document.getElementById("btnLimpiar").addEventListener('click', function () {
  let res = document.getElementById('lista');
  res.innerHTML = "";
});
